package com.divae.ageto.barcode.reader.prototype;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;

public class BarcodeScannerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_scanner);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Button closeButton = (Button) findViewById(R.id.closeButton);
        final TextView scannedText = (TextView) findViewById(R.id.scannedText);
        assert closeButton != null;
        assert scannedText != null;

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCameraPreview();
            }
        });

        showCameraPreview();
    }

    private void showCameraPreview() {
        IntentIntegrator intentIntegrator = new IntentIntegrator(this);
        intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        intentIntegrator.setPrompt("scan");
        intentIntegrator.setCameraId(0);
        intentIntegrator.setBarcodeImageEnabled(false);
        intentIntegrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //if (requestCode == externalIntents.scanBarcode.ordinal()) {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            Toast.makeText(getApplicationContext(), "done.", Toast.LENGTH_SHORT).show();

            if (result == null) {
                // scan has been cancelled
                return;
            }

            final TextView scannedText = (TextView) findViewById(R.id.scannedText);
            assert scannedText != null;
            Toast.makeText(getApplicationContext(), "done2.", Toast.LENGTH_SHORT).show();
            scannedText.setText(result.getContents());
            Toast.makeText(getApplicationContext(), result.getContents(), Toast.LENGTH_SHORT).show();
        //}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_barcode_scanner, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private enum externalIntents {
        scanBarcode
    }
}
